import { login, createVisit, getVisits, editVisit } from './api/ajax.js';
import { LogInBtn, Modal, LoginForm, VisitForm, VisitBtn, VisitList, CreateEditVisitModal } from './calses.js';
import { checkToken } from './function.js';


document.addEventListener('DOMContentLoaded', function() {
    const loginButton = new LogInBtn('loginBtn', 'login_modal');
    loginButton.render()

    const loginForm = new LoginForm(login).renderlogIn()
    const loginModal = new Modal(loginForm, 'Login', 'login_modal')
    const visitForm = new VisitForm(createVisit, editVisit)
    const visitModal = new CreateEditVisitModal(visitForm, 'Create Visit', 'create_visit_modal')
    visitForm.renderVisitForm(visitModal)
    const createVisitBtn = new VisitBtn('createVisitBtn', visitModal).render();
    const list = new VisitList('visits-list', visitModal).render()
    



    checkToken(loginModal);
});


