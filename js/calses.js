import { login, getVisits, deleteVisit, editVisit } from "./api/ajax.js";
class BackDrop{
    backdropRef
    insertBefore

    constructor(insertBefore) {
        this.insertBefore = insertBefore
    }

    remove(){
       this.backdropRef?.remove()
    }

    show(){
        if (!document.getElementsByClassName('backdrop').item(0)) {
        this.backdropRef = document.createElement('button')
        this.backdropRef.classList.add('backdrop')
        this.insertBefore?.before(this.backdropRef)
        }
    }

}

class Modal {
    constructor(content, title, ref) {
        this.content = content;
        this.title = title;
        this.ref = ref;
        this.renderModal()
        }

    close() {
        const modal =  document.getElementById(this.ref)
        modal.classList.remove('active')
        this.backDrop?.remove()
        
        
        
    }

    open() {
        const modal =  document.getElementById(this.ref)
        modal.classList.add('active')
        this.backDrop?.show()
    }

    toggle() {
        const modal =  document.getElementById(this.ref)
        modal.classList.toggle('active')
        if(modal.classList.contains('active')){
            this.backDrop.remove()


        }else{
            this.backDrop.show()

        }
    }

    renderModal() {
        
        const modal = document.getElementById(this.ref);// Отримуємо елемент modal
        this.backDrop = new BackDrop(modal);
        

        modal.insertAdjacentHTML('beforeend', `
            <button class="close_btn">&#10006;</button>
                <h2>${this.title}</h2>
                <div id="content"></div>
        `);
        const closeBtn = modal.getElementsByClassName("close_btn")[0]
        closeBtn.addEventListener('click', () => {
            this.close()
        })
        const contentArea = document.getElementById("content")
        contentArea.replaceWith(this.content)
    }
}

class CreateEditVisitModal extends Modal {
    form
    constructor(formRef, title, ref) {
        super(formRef.form, title,ref)
        this.form = formRef
       
    }

    open(editVisit) {
        this.form.renderVisitForm(this, !!editVisit, editVisit )

        const modal =  document.getElementById(this.ref)
        modal.classList.add('active')
        this.backDrop?.show()
    }

    toggle(editVisit) {
        this.form
        const modal =  document.getElementById(this.ref)
        modal.classList.toggle('active')
        if(modal.classList.contains('active')){
            this.backDrop.remove()


        }else{
            this.backDrop.show()

        }
    }
}

class LogInBtn {
    constructor(ref, modalRef) {
        this.ref = ref;
        this.modalRef = modalRef;
    }

    render() {
        const btn = document.getElementById(this.ref);
        const modal = document.getElementById(this.modalRef);
        btn.onclick = () => {
         modal.classList.add('active');
        };

    }
}




class VisitBtn{
    constructor(ref, modalRef){
        this.ref = ref;
        this.modalRef = modalRef;
    }
    render(){
        const btn = document.getElementById(this.ref);
        btn.onclick = () => {
            this.modalRef.open()
           };
    }
}


class LoginForm {
    constructor(submitFunc,) {
        this.submitFunc = submitFunc;
        
    }

    renderlogIn() {
        this.form = document.createElement('form');
        this.form.innerHTML = `
            <input name="email" type="text" placeholder="email">
            <input name="password" type="password" placeholder="password"> <!-- Виправлено type -->
            <button type="submit" class="submit_button">Submit</button>
        `;

        this.form.addEventListener('submit', (event) => {
            event.preventDefault();
            const formData = new FormData(this.form);
            this.submitFunc(formData.get('email').toString(), formData.get('password').toString());
        });

        return this.form;
    }
}

class VisitForm {
    form = document.createElement('form')
    constructor(createSubmitFunc, editSubmitFunc){
        this.doctorSelect = null;
        this.doctorFields = null;
        this.editSubmitFunc = editSubmitFunc;
        this.createSubmitFunc = createSubmitFunc;
    }

     renderVisitForm(modal, isEditMode, editedVisit) {
        if (!isEditMode) {
            isEditMode = false
        }

        // Створення модального вікна та його DOM-структури
        this.form.addEventListener('submit', async event =>{
            event.preventDefault()
            const formData = new FormData(this.form);

            const visit = {
                doctor: formData.get('doctorSelect'),
                bp: formData.get('bp'),
                age: formData.get('age'),
                weight: formData.get('weight'),
                history: formData.get('history'),
                lastvisit: formData.get('lastvisit'),
                fullname: formData.get('fullname'),
                purpose: formData.get('purpose'),
                description: formData.get('description'),
                priority: formData.get('prioritySelect'),

            }
            this.form.querySelector('.submit_button').textContent = 'loading...'

            if (isEditMode) {
                await this.editSubmitFunc(visit.id, visit)

            } else {
                await this.createSubmitFunc(visit)
            }
            this.form.querySelector('.submit_button').textContent = 'Complete!'
            modal.close()

            

        })
        
        this.form.innerHTML = `
                
                <label for="doctorSelect">Choose the doctor:</label>
                <select name="doctorSelect">
                    <option ${isEditMode ? editedVisit.doctor === 'cardiologist' && 'selected' : ''} value="cardiologist">Cardiologist
                    </option>
                    <option ${isEditMode ? editedVisit.doctor === 'dentist' && 'selected' : ''} value="dentist">Dentist</option>
                    <option ${isEditMode ? editedVisit.doctor === 'therapist' && 'selected' : ''} value="therapist">Therapist</option>
                </select>
                <label for="fullname">Full Name:</label>
                <input value="${isEditMode ? editedVisit.fullname : ''}" name="fullname"></input>
                <label for="purpose">purpose of visit:</label>
                <input value="${isEditMode ? editedVisit.purpose : ''}"  name="purpose"></input>
                <label  for="description">description of visit:</label>
                <input value="${isEditMode ? editedVisit.fullname : ''}" name="description"></input> 
                <label for="prioitySelect">Choose the priority:</label>
                <select  name="prioritySelect">
                    <option ${isEditMode ? editedVisit.prioritySelect === 'ordinary' && 'selected' : ''} value="ordinary">Ordinary
                    </option>
                    <option ${isEditMode ? editedVisit.prioritySelect === 'urgent' && 'selected' : ''} value="urgent">Urgent</option>
                    <option ${isEditMode ? editedVisit.prioritySelect === 'priority' && 'selected': ''} value="priority">Priority</option>
                </select>
                <button  class="submit_button button" id="createButton" type="submit">Create Visit</button>
        `;
    
    const select = this.form.querySelector('[name="doctorSelect"]')
    const target = this.form.querySelector('[name="prioritySelect"]')


    select.addEventListener('change', event => this.showOptionalFields(event, target, isEditMode, editedVisit))
    this.showOptionalFields(null, target, isEditMode, editedVisit)
    return this.form;
    }

     showOptionalFields(event, target, isEditMode, visit) {
        let selected = event?.target?.value
    
        if (!selected) {
            selected = this.form.querySelector('[name="doctorSelect"]').value
        }
       
            const inputs = this.form.querySelectorAll('.optional-input')
            const labels = this.form.querySelectorAll('.optional-label')
            inputs.forEach((input) => {
                input.remove()
            })
            labels.forEach((label) => {
                label.remove()
            })
            
            
    
            if(selected ==='cardiologist'){
                target.insertAdjacentHTML("afterend",`
                <label class="caridologist optional-label" for="bp">BP:</label>
                <input value="${isEditMode ? visit.bp : ''}" class="caridologist optional-input" name="bp"></input>
                <label class="caridologist optional-label" for="age">Age:</label>
                <input value="${isEditMode ? visit.age: ''}" class="caridologist optional-input" name="age"> </input>
                <label class="caridologist optional-label" for="weight">Weight:</label>
                <input value="${isEditMode ? visit.weight: ''}" class="caridologist optional-input" name="weight"> </input>
                <label  class="caridologist optional-label" for="history">History:</label>
                <input value="${isEditMode ? visit.history: ''}" class="caridologist optional-input" name="history"> </input>
                `  ) 
            } 
            if(selected === 'dentist'){
                target.insertAdjacentHTML("afterend",`
                <label class="dentist optional-label" for="lastvisit">Last visit:</label>
                <input value="${isEditMode ? visit.lastvisit: ''}" name="lastvisit" class = "dentist optional-input " > </input>
                
                `  ) 
            }if(selected === 'therapist'){
                target.insertAdjacentHTML("afterend",`
                <label class="therapist optional-label" for="age">Age:</label>
                <input value="${isEditMode ? visit.age: ''}"  name="age" class = "therapist optional-input"> </input>
                
                `  ) 
            }
            
            
        }
    
    

    
}

class VisitCard {
    element;
    constructor(visit,  modalRef){
        this.visit = visit
        this.modalRef = modalRef;
        
    }

    render() {
    this.element = document.createElement('li')
    this.element.classList.add("list_item")
    this.element.innerHTML = `
    <button class="remove_btn">&#10006;</button>
    <h2>${this.visit.fullname}</h2>
    <h3>${this.visit.doctor}</h3>
    <details><summary>read more</summary>
    <div>
    <p>BP: ${this.visit.bp} </p>
    <p>age: ${this.visit.age} </p>
    <p>last visit: ${this.visit.lastvisit} </p>
    <p>weight: ${this.visit.weight} </p>
    <p>history: ${this.visit.history} </p>
    </div>
    </details>
    <button class="edit_btn">Edit</button>

    `
    const removeBtn = this.element.getElementsByClassName("remove_btn")[0]
            removeBtn.addEventListener('click', () => {
                this.remove()
            })
    const editBtn = this.element.getElementsByClassName("edit_btn")[0]
            editBtn.addEventListener('click', () => this.edit());
    
        return this.element
    }

    async remove() {
        const removeVisit = await deleteVisit(this.visit.id)
        
        this.element.remove()
    }

    edit() {
         this.modalRef.open(this.visit)
    }

}

class VisitList { 
    constructor(ref, modal) {
        this.ref = ref
        this.modal = modal
        this.element.classList.add("card_list")
    }
   element = document.createElement('ul')
  
 
    list = []
    
    async render(){
        const data = await getVisits()
        data.forEach(item => this.addCard(item))
        document.getElementById(this.ref).replaceWith(this.element)
    }

    addCard(visit){
        const newCard = new VisitCard(visit, this.modal).render()
        
        this.list.push( newCard)
        this.element.appendChild(newCard)

    }
    removeCard(){
        
    }
}


export { LogInBtn, Modal, LoginForm, VisitBtn, VisitForm, VisitCard, VisitList, CreateEditVisitModal};

const loginFunc = (email, password) => {
    // Логіка авторизації
    console.log('Email:', email);
    console.log('Password:', password);
};

const logInButton = new LogInBtn('loginBtn', 'login_modal');
logInButton.render(); // Викликаємо render() для кнопки "Log In"
