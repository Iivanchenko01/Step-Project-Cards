import { checkToken } from "../function.js";
import {LOGIN_TOKEN_STORAGE_KEY} from "../constant.js";
async function login( email, password) {
  return fetch("https://ajax.test-danit.com/api/v2/cards/login", {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({ email , password  })
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Сетевой запрос не удался');
    }
    return response.text(); 
  })
  .then(data => {
    console.log(data);
    
    window.localStorage.setItem(LOGIN_TOKEN_STORAGE_KEY, data);
    checkToken()
    return data; 
  })
  .catch(error => {
    console.error('Ошибка:', error);
    
  });
}

async function createVisit (visit){
  const token = window.localStorage.getItem(LOGIN_TOKEN_STORAGE_KEY)
  return fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  },
  body: JSON.stringify(visit)
})
  .then(response => response.json())
  .then(response => console.log(response))
}
async function getVisits (){
  const token = window.localStorage.getItem(LOGIN_TOKEN_STORAGE_KEY)

 return fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  },


 })
 .then(response => response.json())
 
}

async function deleteVisit(id ) {
  const token = window.localStorage.getItem(LOGIN_TOKEN_STORAGE_KEY)

  fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
  method: 'DELETE',
  headers: {

    'Authorization': `Bearer ${token}`
  },
})
}

async function editVisit(id, visit) {
  return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  },
  body: JSON.stringify({
    title: visit.title,
    description: visit.description,
    doctor: visit.doctor,
    bp: visit.bp,
    age: visit.age,
    weight: visit.age,
    fullname: visit.fullname,
    lastvisit: visit.lastvisit,
    purpose: visit.purpose,
    priority: visit.priority,
    history: visit.history,

  })
})
  .then(response => response.json())
}

export { login, createVisit, getVisits, deleteVisit, editVisit};
