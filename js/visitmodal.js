class VisitModal {
    constructor() {
        this.modal = null;
        this.doctorSelect = null;
        this.doctorFields = null;

        this.initModal();
        this.initEventListeners();
    }

    initModal() {
        // Створення модального вікна та його DOM-структури
        this.modal = document.createElement('div');
        this.modal.className = 'modal';
        this.modal.innerHTML = `
            
                <span class="close">&times;</span>
                <h2>Створити візит</h2>
                <label for="doctorSelect">Виберіть лікаря:</label>
                <select id="doctorSelect">
                    <option value="cardiologist">Кардіолог</option>
                    <option value="dentist">Стоматолог</option>
                    <option value="therapist">Терапевт</option>
                </select>
                <input id="doctorFields"></input>
                <button id="createButton">Створити</button>
                <button class="closeButton">Закрити</button>
            
        `;
        document.body.appendChild(this.modal);

        // Знаходимо посилання на важливі DOM-елементи
        this.doctorSelect = document.getElementById('doctorSelect');
        this.doctorFields = document.getElementById('doctorFields');
    }

    initEventListeners() {
        const closeButton = this.modal.querySelector('.close');
        closeButton.addEventListener('click', () => this.closeModal());

        const createButton = this.modal.querySelector('#createButton');
        createButton.addEventListener('click', () => this.createVisit());

        const closeButton2 = this.modal.querySelector('.closeButton');
        closeButton2.addEventListener('click', () => this.closeModal());

        this.doctorSelect.addEventListener('change', () => this.renderDoctorFields());
    }

    renderDoctorFields() {
        const selectedDoctor = this.doctorSelect.value;
        // Додати логіку для відображення полів для обраного лікаря
        
    }

    createVisit() {
        // Додати логіку для створення візиту
        // ...
    }

    showModal() {
        this.modal.style.display = 'block';
    }

    closeModal() {
        this.modal.style.display = 'none';
    }
}

const visitModal = new VisitModal();


export{VisitModal}